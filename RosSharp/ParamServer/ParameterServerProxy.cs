﻿#if NETFRAMEWORK
using CookComputing.XmlRpc;
#else
using Horizon.XmlRpc.Client;
using Horizon.XmlRpc.Core;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosSharp.ParamServer
{
    /// <summary>
    ///   Asynchronous call Proxy for ParameterServer API
    /// </summary>
    public interface ParameterServerProxy : IXmlRpcProxy, IParameterServer
    {

    }
}
