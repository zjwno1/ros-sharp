﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosSharp.ParamServer
{
    public enum StatusCode
    {
        Error = -1,
        Failure = 0,
        Success = 1
    }
}
