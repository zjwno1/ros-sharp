﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsgGen
{
    public class PInvokeAllUpInYoGrill
    {
        public static bool Is64BitProcess
        {
            get { return Environment.Is64BitOperatingSystem; }
        }

        public static bool Is64BitOperatingSystem
        {
            get
            {
                return Environment.Is64BitOperatingSystem;
            }
        }
    }
}
