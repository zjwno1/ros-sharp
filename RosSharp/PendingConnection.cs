﻿// File: PendingConnection.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

using System;
using RosSharp.XmlRPC;


namespace RosSharp
{
    public class PendingConnection : AsyncXmlRpcConnection, IDisposable
    {
        public string RemoteUri;
        private int _failures;
        private XmlRpcValue chk;
        public XmlRpcClient client;
        public Subscription parent;

        //public XmlRpcValue stickaroundyouwench = null;
        public PendingConnection(XmlRpcClient client, Subscription s, string uri, XmlRpcValue chk)
        {
            this.client = client;
            this.chk = chk;
            parent = s;
            RemoteUri = uri;
        }

        #region IDisposable Members

        public void Dispose()
        {
            chk = null; //.Dispose();
            client.Dispose();
            client = null;
        }

        #endregion

        public int Failures
        {
            get { return _failures; }
            set { _failures = value; }
        }

        public override void AddToDispatch(XmlRpcDispatch disp)
        {
            if (disp == null)
                return;
            if (Check())
                return;
            disp.AddSource(client, (XmlRpcDispatch.EventType.WritableEvent | XmlRpcDispatch.EventType.Exception));
        }

        public override void RemoveFromDispatch(XmlRpcDispatch disp)
        {
            disp.RemoveSource(client);
        }

        public override bool Check()
        {
            if (parent == null)
                return false;
            if (client.ExecuteCheckDone(chk))
            {
                parent.PendingConnectionDone(this, chk);
                return true;
            }
            return false;
        }
    }
}