﻿// File: SingleSubscriberPublisher.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

using Messages; 


namespace RosSharp
{
    public class SingleSubscriberPublisher
    {
        public SubscriberLink link;

        public SingleSubscriberPublisher(SubscriberLink link)
        {
            this.link = link;
        }

        public string Topic
        {
            get { return link.topic; }
        }

        public string SubscriberName
        {
            get { return link.destination_caller_id; }
        }

        public void Publish<M>(M message) where M : IRosMessage, new()
        {
            link.EnqueueMessage(new MessageAndSerializerFunc(message, message.Serialize, true, true));
        }
    }
}