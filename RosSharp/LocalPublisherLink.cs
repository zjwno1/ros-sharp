﻿// File: LocalPublisherLink.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016


using System;
using System.Collections;
using Messages;
using m = Messages.std_msgs;


namespace RosSharp
{
    public class LocalPublisherLink : PublisherLink, IDisposable
    {
        private object drop_mutex = new object();
        private bool dropped;
        private LocalSubscriberLink publisher = null;

        public LocalPublisherLink(Subscription parent, string xmlrpc_uri) : base(parent, xmlrpc_uri)
        {
        }

        public new string TransportType
        {
            get { return "INTRAPROCESS"; /*lol... pwned*/ }
        }

        public void SetPublisher(LocalSubscriberLink pub_link)
        {
            lock (parent)
            {
                IDictionary header = new Hashtable();
                header["topic"] = parent.name;
                header["md5sum"] = parent.md5sum;
                header["callerid"] = ThisNode.Name;
                header["type"] = parent.datatype;
                header["tcp_nodelay"] = "1";
                SetHeader(new Header {Values = header});
            }
        }

        public override void Drop()
        {
            lock (drop_mutex)
            {
                if (dropped) return;
                dropped = true;
            }


            if (publisher != null)
            {
                publisher.Drop();
            }

            lock (parent)
                parent.RemovePublisherLink(this);
        }

        public void HandleMessage<T>(T m, bool ser, bool nocopy) where T : IRosMessage, new()
        {
            stats.messages_received++;
            if (m.Serialized == null)
            {
                //ignore stats to avoid an unnecessary allocation
            }
            else
            {
                stats.bytes_received += (ulong) m.Serialized.Length;
            }
            if (parent != null)
                lock (parent)
                    stats.drops += parent.HandleMessage(m, ser, nocopy, m.connection_header, this);
        }

        public void GetPublishTypes(ref bool ser, ref bool nocopy, MsgTypes mt)
        {
            lock (drop_mutex)
            {
                if (dropped)
                {
                    ser = false;
                    nocopy = false;
                    return;
                }
            }
            if (parent != null)
                lock (parent)
                {
                    parent.GetPublishTypes(ref ser, ref nocopy, mt);
                }
            else
            {
                ser = true;
                nocopy = false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}