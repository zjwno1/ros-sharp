﻿// File: Service.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

using System;
using System.Collections;
using System.Threading;


namespace RosSharp
{
    public static class Service
    {
        public static bool Exists(string service_name, bool print_failure_reason)
        {
            string mapped_name = Names.Resolve(service_name);

            string host = "";
            int port = 0;

            if (ServiceManager.Instance.LookUpService(mapped_name, ref host, ref port))
            {
                TcpTransport transport = new TcpTransport();

                IDictionary m = new Hashtable
                {
                    {"probe", "1"},
                    {"md5sum", "*"},
                    {"callerid", ThisNode.Name},
                    {"service", mapped_name}
                };

                byte[] headerbuf = null;
                int size = 0;
                Header h = new Header();
                h.Write(m, ref headerbuf, ref size);

                if (transport.Connect(host, port))
                {
                    byte[] sizebuf = BitConverter.GetBytes(size);

                    transport.Write(sizebuf, 0, sizebuf.Length);
                    transport.Write(headerbuf, 0, size);

                    return true;
                }
                if (print_failure_reason)
                {
                    ROS.Info("waitForService: Service[{0}] could not connect to host [{1}:{2}], waiting...", mapped_name, host, port);
                }
            }
            else if (print_failure_reason)
            {
                ROS.Info("waitForService: Service[{0}] has not been advertised, waiting...", mapped_name);
            }
            return false;
        }

        public static bool WaitForService(string service_name, TimeSpan ts)
        {
            string mapped_name = Names.Resolve(service_name);
            DateTime start_time = DateTime.Now;
            bool printed = false;
            while (ROS.ok)
            {
                if (Exists(service_name, !printed))
                {
                    break;
                }
                printed = true;
                if (ts >= TimeSpan.Zero)
                {
                    if (DateTime.Now.Subtract(start_time) > ts)
                        return false;
                }
                Thread.Sleep(ROS.WallDuration);
            }

            if (printed && ROS.ok)
            {
                ROS.Info("waitForService: Service[{0}] is now available.", mapped_name);
            }
            return true;
        }

        public static bool WaitForService(string service_name, int timeout)
        {
            return WaitForService(service_name, TimeSpan.FromMilliseconds(timeout));
        }
    }
}