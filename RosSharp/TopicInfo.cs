﻿// File: TopicInfo.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

namespace RosSharp
{
    public class TopicInfo
    {
        public TopicInfo(string name, string data_type)
        {
            // TODO: Complete member initialization
            this.Name = name;
            this.DataType = data_type;
        }

        public string DataType { get; set; }
        public string Name { get; set; }
    }
}