﻿// File: ServiceClientLink.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

using System;
using System.Collections;
using System.IO;
using System.Text;
using Messages;
using String = Messages.std_msgs.String;


namespace RosSharp
{
    public class IServiceClientLink
    {
        public Connection connection;
        public IServicePublication parent;
        public bool persistent;


        public bool Initialize(Connection conn)
        {
            connection = conn;
            connection.DroppedEvent += OnConnectionDropped;
            return true;
        }

        public bool HandleHeader(Header header)
        {
            if (!header.Values.Contains("md5sum") || !header.Values.Contains("service") || !header.Values.Contains("callerid"))
            {
                string bbq = "Bogus tcpros header. did not have required elements: md5sum, service, callerid";
                ROS.Error(bbq);
                connection.SendHeaderError(ref bbq);
                return false;
            }
            string md5sum = (string)header.Values["md5sum"];
            string service = (string)header.Values["service"];
            string client_callerid = (string)header.Values["client_callerid"];

            if (header.Values.Contains("persistent") && ((string)header.Values["persistent"] == "1" || (string)header.Values["persistent"] == "true"))
                persistent = true;

            ROS.Debug("Service client [{0}] wants service [{1}] with md5sum [{2}]", client_callerid, service, md5sum);
            IServicePublication isp = ServiceManager.Instance.LookupServicePublication(service);
            if (isp == null)
            {
                string bbq = string.Format("received a tcpros connection for a nonexistent service [{0}]", service);
                ROS.Error(bbq);
                connection.SendHeaderError(ref bbq);
                return false;
            }

            if (isp.md5sum != md5sum && md5sum != "*" && isp.md5sum != "*")
            {
                string bbq = "client wants service " + service + " to have md5sum " + md5sum + " but it has " + isp.md5sum + ". Dropping connection";
                ROS.Error(bbq);
                connection.SendHeaderError(ref bbq);
                return false;
            }

            if (isp.isDropped)
            {
                string bbq = "received a tcpros connection for a nonexistent service [" + service + "]";
                ROS.Error(bbq);
                connection.SendHeaderError(ref bbq);
                return false;
            }

            parent = isp;
            IDictionary m = new Hashtable();
            m["request_type"] = isp.req_datatype;
            m["response_type"] = isp.res_datatype;
            m["type"] = isp.datatype;
            m["md5sum"] = isp.md5sum;
            m["callerid"] = ThisNode.Name;

            connection.WriteHeader(m, OnHeaderWritten);

            isp.AddServiceClientLink(this);
            return true;
        }

        public virtual void ProcessResponse(string error, bool success)
        {
            String msg = new String(error);
            msg.Serialized = msg.Serialize();
            byte[] buf = new byte[msg.Serialized.Length + 1];
            buf[0] = (byte)(success ? 0x01 : 0x00);
            msg.Serialized.CopyTo(buf, 1);
            connection.Write(buf, buf.Length, OnResponseWritten, true);
        }

        public virtual void ProcessResponse(IRosMessage msg, bool success)
        {
            msg.Serialized = msg.Serialize();
            using (var ms = new MemoryStream())
            {
                using (var writer = new BinaryWriter(ms))
                {
                    writer.Write((byte)(success ? 0x01 : 0x00));
                    writer.Write(msg.Serialized.Length);
                    writer.Write(msg.Serialized);
                    byte[] buf = ms.ToArray();
                    connection.Write(buf, buf.Length, OnResponseWritten, true);
                }
            }
        }

        public virtual void Drop()
        {
            if (connection.sendingHeaderError)
                connection.DroppedEvent -= OnConnectionDropped;
            else
                connection.Drop(Connection.DropReason.Destructing);
        }

        private void OnConnectionDropped(Connection conn, Connection.DropReason reason)
        {
            if (conn != connection || parent == null) return;
            lock (parent)
            {
                parent.RemoveServiceClientLink(this);
            }
        }


        public virtual bool OnRequestLength(Connection conn, byte[] buffer, int size, bool success)
        {
            if (!success) return false;

            if (conn != connection || size != 4)
                throw new Exception("Invalid request length read");

            int len = BitConverter.ToInt32(buffer, 0);
            if (len > 1000000000)
            {
                ROS.Error("A message over a gigabyte was predicted... stop... being... bad.");
                connection.Drop(Connection.DropReason.Destructing);
                return false;
            }
            connection.Read(len, OnRequest);
            return true;
        }

        public virtual bool OnRequest(Connection conn, byte[] buffer, int size, bool success)
        {
            if (!success) return false;
            if (conn != connection)
                throw new Exception("WRONG CONNECTION!");

            if (parent != null)
                lock (parent)
                {
                    parent.ProcessRequest(ref buffer, size, this);
                    return true;
                }
            return false;
        }

        public virtual bool OnHeaderWritten(Connection conn)
        {
            connection.Read(4, OnRequestLength);
            return true;
        }

        public virtual bool OnResponseWritten(Connection conn)
        {
            if (conn != connection)
                throw new Exception("WRONG CONNECTION!");

            if (persistent)
                connection.Read(4, OnRequestLength);
            else
                connection.Drop(Connection.DropReason.Destructing);
            return true;
        }
    }
}