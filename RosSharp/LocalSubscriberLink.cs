﻿// File: LocalSubscriberLink.cs
// Project: ROS_C-Sharp
// 
// ROS.NET
// Eric McCann <emccann@cs.uml.edu>
// UMass Lowell Robotics Laboratory
// 
// Reimplementation of the ROS (ros.org) ros_cpp client in C#.
// 
// Created: 04/28/2015
// Updated: 02/10/2016

using System;
using Messages;
using m = Messages.std_msgs;
using gm = Messages.geometry_msgs;
using nm = Messages.nav_msgs;

namespace RosSharp
{
    public class LocalSubscriberLink : SubscriberLink, IDisposable
    {
        private object drop_mutex = new object();
        private bool dropped;
        private LocalPublisherLink subscriber;

        public LocalSubscriberLink(Publication pub)
        {
            parent = pub;
            topic = parent.Name;
        }

        public string TransportType
        {
            get { return "INTRAPROCESS"; /*lol... pwned*/ }
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        public void SetSubscriber(LocalPublisherLink pub_link)
        {
            subscriber = pub_link;
            connection_id = ConnectionManager.Instance.GetNewConnectionID();
            destination_caller_id = ThisNode.Name;
        }

        internal override void EnqueueMessage(MessageAndSerializerFunc holder)
        {
            lock (drop_mutex)
            {
                if (dropped) return;
            }

            if (subscriber != null)
                subscriber.HandleMessage(holder.msg, holder.serialize, holder.nocopy);
        }


        public override void Drop()
        {
            lock (drop_mutex)
            {
                if (dropped) return;
                dropped = true;
            }
            if (subscriber != null)
            {
                subscriber.Drop();
            }

            lock (parent)
                parent.RemoveSubscriberLink(this);
        }


        public override void GetPublishTypes(ref bool ser, ref bool nocopy, MsgTypes mt)
        {
            lock (drop_mutex)
            {
                if (dropped)
                {
                    ser = false;
                    nocopy = false;
                    return;
                }
            }
            subscriber.GetPublishTypes(ref ser, ref nocopy, mt);
        }
    }
}