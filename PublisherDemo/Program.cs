﻿using RosSharp;
using System;
using System.Threading;

namespace PublisherDemo
{
    class Program
    {
        static Thread thread;
        static void Main(string[] args)
        {
            ROS.ROS_MASTER_URI = "http://192.168.20.132:11311";
            ROS.ROS_HOSTNAME = "192.168.20.1";
            ROS.ROS_IP = "192.168.20.132";

            string nodeName = "bbb";
            ROS.Init(nodeName);
            NodeHandle nh = new NodeHandle();
            Publisher<Messages.std_msgs.String> pub = nh.Advertise<Messages.std_msgs.String>("/chatter", 1, false);
            thread = new Thread(() =>
            {
                int i = 0;
                while (ROS.ok)
                {
                    i++;
                    pub.Publish(new Messages.std_msgs.String { data = "Message" + i });
                    if (i == 65536)
                        i = 0;
                    Thread.Sleep(1000);
                }
            });
            thread.Start();
            Console.CancelKeyPress += Console_CancelKeyPress;
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            ROS.Shutdown();
            ROS.WaitForShutdown();
        }
    }
}
