﻿using RosSharp.MsgGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsgGen
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputDir = AppDomain.CurrentDomain.BaseDirectory + "common_msgs/";
            string outputdir = AppDomain.CurrentDomain.BaseDirectory + "Messages";
            Console.WriteLine("开始生成ROS Messages...\n");
            GenerateHelper.GenerateMsgAndSrv(inputDir, outputdir);
            Console.WriteLine("生成完成");
            Console.ReadLine();
        }
    }
}
