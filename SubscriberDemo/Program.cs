﻿using RosSharp;
using System;

namespace SubscriberDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ROS.ROS_MASTER_URI = "http://192.168.20.132:11311";
            ROS.ROS_HOSTNAME = "192.168.20.1";
            ROS.ROS_IP = "192.168.20.132";

            string nodeName = "aaa";
            ROS.Init(nodeName);
            NodeHandle nh = new NodeHandle();
            Subscriber<Messages.std_msgs.String> sub = nh.Subscribe<Messages.std_msgs.String>("/chatter", 10, subCallback);
            Console.CancelKeyPress += Console_CancelKeyPress;
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            ROS.Shutdown();
            ROS.WaitForShutdown();
        }

        private static void subCallback(Messages.std_msgs.String argument)
        {
            Console.WriteLine(argument.data);
        }
    }
}
