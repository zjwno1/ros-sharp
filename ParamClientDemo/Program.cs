﻿using RosSharp;
using RosSharp.ParamServer;
using RosSharp.XmlRPC;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ParamClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ROS.ROS_MASTER_URI = "http://192.168.20.132:11311";
            ROS.ROS_HOSTNAME = "192.168.20.1";
            ROS.ROS_IP = "192.168.20.132";
            IDictionary remappings;
            RemappingHelper.GetRemappings(out remappings);
            Network.Init(remappings);
            Master.Init(remappings);
            ThisNode.Init("", remappings, (int)(InitOption.AnonymousName | InitOption.NoRousout));
            Param.Init(remappings);
            //获得所有参数
            List<string> list = Param.List();

            foreach (string s in list)
            {
                Console.WriteLine(s);
            }
            //获取参数值
            foreach (string s in list)
            {
                bool flag = Param.Has(s);
                if (!flag)
                {
                    Console.WriteLine("不存在参数{0}", s);
                    continue;
                }
                object obj = Param.Get(s);
                if (obj == null)
                {
                    Console.WriteLine("获取参数失败");
                    continue;
                }

                Console.WriteLine("参数：{0},值：{1}", s, obj);
            }
            //设置参数值
            //Param.set(key, value);
            //Param.del(key)
            Console.ReadKey();
        }
    }
}
