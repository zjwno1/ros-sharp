﻿using Messages.roscpp_tutorials;
using RosSharp;
using System;
using System.IO;

namespace ServiceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ROS.ROS_MASTER_URI = "http://192.168.20.132:11311";
            ROS.ROS_HOSTNAME = "192.168.20.1";
            ROS.ROS_IP = "192.168.20.132";
            ROS.Init("ServiceServerTest");
            NodeHandle nodeHandle = new NodeHandle();
            ServiceServer server = nodeHandle.AdvertiseService<TwoInts.Request, TwoInts.Response>("/add_two_ints", addition);
            Console.CancelKeyPress += Console_CancelKeyPress;
        }

        private static bool addition(TwoInts.Request req, ref TwoInts.Response resp)
        {
            resp.sum = req.a + req.b;
            return true;
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            ROS.Shutdown();
        }
    }
}
